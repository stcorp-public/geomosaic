# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.8.1] - 2021-05-19
### Added
- README section on possible Geomosaic improvements(retrieved from our previous Mosaic repository).

### Changed
- Changed method of padding cloud pixels to the more efficient `binary_dilation` and `generate_binary_structure`.
- Linted code base.

### Fixed
- Added missing parenthesis in the unit testing.

## [1.8.0] - 2021-02-18
### Changed
- Unclassified pixels are no longer considered "good".
- The selection process has been changed to select unclassified pixels last,
  and thus preferred slightly ahead of classes dark, shadow.

### Fixed
- A major bug that caused `no_data` pixels to be selected has been resolved.

## [1.7.1] - 2020-11-24
### Fixed
- A bug resulting in incorrect tilemaps for runs with more than 255 input
  datasets

## Changed
- Tilemap data type is now uint16
- In order to ensure that tilemap metadata can be used to determine the amount
  of input data processed, all scene IDs are now written to tilemap metadata,
  including the ones that will be skipped later.

## [1.7.0] - 2020-11-06
### Added
- A plugin for performing mosaicing on Landsat 4-7 SR datasets (LEDAPS)
- Cloud padding capability for all three implemented datasets

### Fixed
- A bug resulting in slightly incorrect 'good pixel' score for Landsat 8 datasets
- A bug in `environment.yml` resulting in an incomplete conda environment
- Version incompatibility resulting in a broken `docker build` process

### Changed
- The default band indices for Landsat 8 products, such that the default
  corresponds to the latest relevant Product Guide.
