FROM ubuntu:18.04 as build

LABEL maintainer Robin Skahjem-Eriksen (skahjem-eriksen@stcorp.no)

ARG SETUPTOOLS_SCM_PRETEND_VERSION

# GDAL 3.0.2 seem to have switched the order of lat/lon values in a dataset's
# geotransform. In particular, this causes incorrect envelope calculation in
# util.get_ds_envelope and then further problems down the road.
RUN apt-get update \
    && apt-get install -y --no-install-recommends software-properties-common \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        gdal-bin=2.2.* \
        libgdal-dev=2.2.* \
        python3-gdal=2.2.* \
        build-essential \
        python3-dev \
        python3-venv

RUN mkdir -p /opt \
    && python3.6 -m venv /opt/venv \
    && /opt/venv/bin/pip install numpy==1.19 \
    && /opt/venv/bin/pip install numba==0.48 \
    && /opt/venv/bin/pip install --global-option=build_ext --global-option="-I/usr/include/gdal" GDAL==$(gdal-config --version)

COPY . /work
RUN cd /work \
    && /opt/venv/bin/python setup.py install

FROM ubuntu:18.04

COPY --from=build /opt/venv /opt/venv

RUN apt-get update \
    && apt-get install -y --no-install-recommends software-properties-common \
    && apt-get update \
    && apt-get install -y --no-install-recommends gdal-bin \
    && rm -rf /var/lib/apt/lists/*

ENV PYTHONUNBUFFERED=1 \
    # Click env requirements
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 

ENTRYPOINT ["/opt/venv/bin/geomosaic"]
