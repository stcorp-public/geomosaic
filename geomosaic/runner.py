"""
Mosaic runner base class
"""

from abc import ABC, abstractmethod
import os
import pathlib
import datetime as dt
import time
import subprocess
from typing import Optional, List, Tuple, Dict, Any, Sequence
from osgeo import gdal  # type: ignore
from osgeo import ogr  # type: ignore
from osgeo import osr  # type: ignore
from osgeo import gdalnumeric  # type: ignore
import numpy as np  # type: ignore
from numba import jit  # type: ignore

from .util import get_ds_envelope, seconds_to_hr

__author__ = "Janis Gailis"
__email__ = "gailis@stcorp.no"

# Enable ogr and gdal exceptions
gdal.UseExceptions()
ogr.UseExceptions()


WGS = "WGS84"


class Runner(ABC):
    def __init__(
        self,
        input_dsets: Optional[List[str]] = None,
        target_date: Optional[dt.datetime] = None,
        weights: Optional[Tuple[int]] = None,
        band_indices: Optional[Tuple[int]] = None,
        cloud_mask_padding_px: Optional[int] = None,
        outfilepath: Optional[pathlib.Path] = None,
        roi: ogr.Geometry = None,
        epsg_rs: int = None,
        date_stddev: int = None,
        repr_path: Optional[pathlib.Path] = None,
        reproject: bool = False,
    ) -> None:
        """
        Initialize the mosaic runner

        :param input_dsets: A list of input datasets
        :param target_date: The desired mosaic target date
        :param weights: [w1, w2, .., 2n]
        :param band_indices: [b1, b2, .., bn]
        :param cloud_mask_padding_px: int
        :param outfilepath: Output file path
        :param roi: Region of interest as an osgeo.ogr.Geometry
        :param epsg_rs: Target EPSG reference system
        :param reproject: Skip datasets that must be reprojected
        """
        if input_dsets is not None:
            self.input_dsets = input_dsets
        else:
            raise ValueError("Input files needed")

        if target_date is None:
            self.target_date = (
                self.date_key(input_dsets[0])
                + (
                    self.date_key(input_dsets[-1])
                    - self.date_key(input_dsets[0])
                )
                // 2
            )
        else:
            self.target_date = target_date

        self.weights = weights
        self.band_indices = band_indices
        self.reproject = reproject
        self.cloud_mask_padding_px = cloud_mask_padding_px

        if outfilepath is not None:
            self.outfilepath = outfilepath
        else:
            raise ValueError("Output filename needed")

        if roi is not None:
            self.roi = roi
        else:
            raise ValueError("ROI needed")

        if epsg_rs is not None:
            self.epsg_rs = epsg_rs
        else:
            raise ValueError("EPSG projection needed")

        if date_stddev is not None:
            self.date_stddev = date_stddev
        else:
            raise ValueError("Date standard deviation needed")

        self.repr_path = repr_path

        # Set reference systems
        self.mos_srs = osr.SpatialReference()
        self.mos_srs.ImportFromEPSG(self.epsg_rs)
        self.roi_srs = osr.SpatialReference()
        self.roi_srs.SetWellKnownGeogCS(WGS)
        self.roi.AssignSpatialReference(self.roi_srs)

    @property
    def weights(self) -> Dict[str, int]:
        return self._weights

    @weights.setter
    def weights(self, weights: Optional[Tuple[int, ...]]) -> None:
        """
        Default weights setter

        :param weights: [target_date, n_pixels, aot]
        """
        if weights is not None:
            self._weights = dict(
                zip(("target_date", "n_pixels", "aot"), weights)
            )
        else:
            raise ValueError("Weights needed")

    @property
    def band_indices(self) -> Dict[str, int]:
        return self._band_indices  # type: ignore

    @band_indices.setter  # type: ignore
    @abstractmethod
    def band_indices(self, value: Optional[Tuple[int, ...]]) -> None:
        """
        Default band indices setter

        :param value: Band indices
        """
        pass

    @staticmethod
    @abstractmethod
    def date_key(filepath: str) -> dt.datetime:
        """
        Extract date from the given filepath for sorting

        :param filepath: Path to dataset
        """
        pass

    @staticmethod
    @abstractmethod
    def get_repr_filename(filename: str, utm_zone: int) -> str:
        """
        Get reprojected file filename

        :param filename: Filename of original file
        :param utm_zone: UTM zone
        """
        pass

    @abstractmethod
    def initialize_outputs(self) -> None:
        """Initialize outputs"""
        self.outputs: Dict[str, Any] = {}

    @abstractmethod
    def calculate_desirability_score(
        self, ds_arr: np.ndarray, ds_date: dt.datetime
    ) -> float:
        """
        Calculate the 'desirability' score for the given dataset.

        :param ds_arr: ndarray containing the dataset values
        :param ds_date: Date associated with this dataset
        """
        pass

    @abstractmethod
    def initialize_dataset(self, filepath: pathlib.Path) -> Dict[str, Any]:
        """
        Initialize a dataset object from representative filepath

        :param filepath: Path to dataset
        """
        pass

    @abstractmethod
    def construct_replacement_mask(
        self, ds: Dict[str, Any]
    ) -> Tuple[np.ndarray, Tuple[int, ...]]:
        """
        Construct a mask of pixels to be replaced in the mosaic from the given
        dataset

        :param ds: Dataset with metadata object
        """
        pass

    @abstractmethod
    def update_mosaic(
        self, ds: Dict[str, Any], mask_subset: np.ndarray
    ) -> None:
        """
        Update the mosaic from the given dataset according to the given mask

        :param ds: Dataset with metadata object
        :param mask_subset: Mask for pixel selection from this dataset
        """
        pass

    def should_add(self, ds_name: str) -> bool:
        """
        Determine if the given dataset should be added to the mosaic, if not,
        log a warning

        :param ds_name: Dataset filename
        :return: True or False
        """
        retval = False

        ds = gdal.Open(ds_name)

        if (
            ds.GetProjection() != self.mos_srs.ExportToWkt()
            and not self.reproject
        ):
            print(
                "The given dataset is not on the target projection and "
                "the --no-reproject flag has been used. Skipping dataset: "
                f"\n{ds_name}."
            )
            return False

        # Check if the dataset overlaps the ROI
        footprint_wkt = ds.GetMetadataItem("FOOTPRINT")
        if footprint_wkt:
            f_ref = osr.SpatialReference()
            f_ref.SetWellKnownGeogCS(WGS)
            footprint = ogr.CreateGeometryFromWkt(
                footprint_wkt, reference=f_ref
            )
            retval = self.roi.Intersects(footprint)
        else:
            ds_envelope = get_ds_envelope(ds)
            retval = self.roi.Intersects(ds_envelope)
        if not retval:
            print(
                "The given dataset does not intersect the desired ROI. "
                f"Skipping dataset: \n{ds_name}"
            )
            ds = None
            return False

        ds = None
        return retval

    def add_tile(self, i: int) -> None:
        """
        Add to the mosaic the contribution of a GeoTiff dataset.

        :param i: Tile index in the timeseries
        """
        ds = self.initialize_dataset(pathlib.Path(self.input_dsets[i]))

        tmap_arr = self.outputs["tmap"]["array"]
        tmap_metadata = self.outputs["tmap"]["meta"]

        print(f"Add dataset [{i+1}/{len(self.input_dsets)}]: {ds['uri']}")
        tmap_metadata[f"tile_{i+1}"] = ds["uri"]

        # Open the GDAL datasets
        for key in ds["dsets"]:
            ds_dict = ds["dsets"][key]
            ds_dict["gdal"] = self.open_dataset(ds_dict["path"])
            if ds_dict["gdal"] is None:
                return

        # Don't do anything if we have an incomplete dataset
        for key in ds["dsets"]:
            ds_dict = ds["dsets"][key]
            mos_dict = self.outputs[key]
            if ds_dict["gdal"].RasterCount != mos_dict["array"].shape[0]:
                ds_dict["gdal"] = None
                print(
                    f"Dataset {ds['uri']} does not seem to have enough"
                    " bands, will not be added to the"
                    " mosaic."
                )
                return

        mask_subset, tmap_indices = self.construct_replacement_mask(ds)
        # Don't waste time updating the mosaics if the mask is all False
        if not np.any(mask_subset):
            print(
                f"No pixels from dataset {ds['uri']} make a good "
                "contribution to the mosaic. Carry on."
            )
            return

        # Update the tilemap
        (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        ) = tmap_indices
        tmap_arr[:, my_min:my_max, mx_min:mx_max] = np.where(
            mask_subset, i + 1, tmap_arr[:, my_min:my_max, mx_min:mx_max]
        )

        self.update_mosaic(ds, mask_subset)

        # Close GDAL datasets
        for key in ds["dsets"]:
            ds_dict = ds["dsets"][key]
            ds_dict["array"] = None
            ds_dict["gdal"] = None

        return

    def save_outputs(self, verbose: bool = False) -> None:
        """
        Save mosaic outputs.

        :param verbose: If should be logged
        """
        if verbose:
            print("Save outputs")
        for key in self.outputs:
            res = self.outputs[key]
            if verbose:
                print(f"Saving: {res['path']}")
            gdalnumeric.SaveArray(res["array"], str(res["path"]))
            res_ds = gdal.Open(str(res["path"]), gdal.GA_Update)
            res_ds.SetGeoTransform(res["geot"])
            res_ds.SetProjection(res["proj"])
            res_ds.SetMetadata(res["meta"])
            res_ds = None

    def open_dataset(self, ds_path: pathlib.Path) -> gdal.Dataset:
        """
        Open the dataset pointed to in the filename, if has to be reprojected,
        check if it's cached, reproject and cache if it's not.

        :param ds_path: Dataset filepath
        :return: GDAL dataset
        """
        ds = gdal.Open(str(ds_path))

        # Check if the dataset has the same coordinate system, as the mosaic
        if ds.GetProjection() == self.mos_srs.ExportToWkt():
            # All is cake and eggs
            return ds

        ds_srs = osr.SpatialReference()
        ds_srs.ImportFromWkt(ds.GetProjection())
        try:
            source_srs = (
                ds_srs.GetAuthorityName(None)
                + ":"
                + ds_srs.GetAuthorityCode(None)
            )
        except TypeError:
            # Can't get EPSG reference
            print("Unable to get EPSG reference")
            return None

        target_srs = (
            self.mos_srs.GetAuthorityName(None)
            + ":"
            + self.mos_srs.GetAuthorityCode(None)
        )
        pix_width = ds.GetGeoTransform()[1]
        pix_height = ds.GetGeoTransform()[5]
        ds = None

        print(f"File: {ds_path.name} must be reprojected")
        if self.repr_path is None:
            repr_path = ds_path.parent.resolve() / "reprojected"
            oldmask = os.umask(000)
            repr_path.mkdir(parents=True, exist_ok=True, mode=0o777)
            os.umask(oldmask)
        else:
            repr_path = self.repr_path

        zone = self.mos_srs.GetAuthorityCode(None)[-2:]
        repr_filename = self.get_repr_filename(ds_path.name, zone)
        repr_path = repr_path / repr_filename
        if repr_path.is_file():
            # Hey, it's cached!
            print("Using cached reprojected file")
            ds = gdal.Open(str(repr_path))
            return ds

        return self.reproject_dataset(
            source_srs, target_srs, pix_width, pix_height, ds_path, repr_path
        )

    def reproject_dataset(
        self,
        source_srs: str,
        target_srs: str,
        pix_width: float,
        pix_height: float,
        ds_path: pathlib.Path,
        repr_path: pathlib.Path,
    ) -> gdal.Dataset:
        """
        Reproject the given dataset

        :param source_srs: Source reference system
        :param target_srs: Target reference system
        :param pix_width: Pixel width
        :param pix_height: Pixel height
        :param ds_path: Full path of the dataset to be reprojected
        :param repr_path: Full path of the reprojected dataset
        """
        print("Reprojecting...")
        # 'near' and 'mode' are the resampling methods best suited to preserve
        # some sanity in the classmap.
        cli: Sequence[str] = [
            "gdalwarp",
            "-s_srs",
            source_srs,
            "-t_srs",
            target_srs,
            "-srcnodata",
            "0",
            "-dstnodata",
            "0",
            "-r",
            "near",
            "-tap",
            "-tr",
            str(pix_width),
            str(pix_height),
            str(ds_path),
            str(repr_path),
        ]
        completed = subprocess.run(cli)
        if completed.returncode == 0:
            ds = gdal.Open(str(repr_path))
            return ds

        print("Reprojection failed, skip this dataset")
        return None

    def process(self, save_intermediate: bool = False) -> None:
        """
        Create a mosaic

        :param save_intermediate: If intermediate output should be saved
        """
        self.initialize_outputs()

        # Add tiles to the mosaic
        for i in range(0, len(self.input_dsets)):
            start_time = time.time()

            if not self.should_add(self.input_dsets[i]):
                continue

            self.add_tile(i)

            end_time = time.time()
            days, hours, minutes, seconds = seconds_to_hr(
                end_time - start_time
            )
            print(
                "Dataset processed in:"
                f" {minutes} minutes, {seconds} seconds"
            )
            if save_intermediate:
                self.save_outputs()

        self.save_outputs(verbose=True)
        return

    @staticmethod
    def pad_bbox(
        bbox: Tuple[float, float, float, float], pixel_size: int
    ) -> Tuple[int, int, int, int]:
        """
        Pad a given bounding box, such that its extents fall on pixel
        boundaries.

        :param bbox: [x_min, x_max, y_min, y_max]
        :param pixel_size: Pixel size in coordinate system units
        :return: [x_min, x_max, y_min, y_max]
        """
        x_min, x_max, y_min, y_max = bbox
        x_min = x_min - (x_min % pixel_size)
        x_max = x_max + pixel_size - (x_max % pixel_size)
        y_min = y_min - (y_min % pixel_size)
        y_max = y_max + pixel_size - (y_max % pixel_size)

        return (int(x_min), int(x_max), int(y_min), int(y_max))


@jit(nopython=True, parallel=True)
def update_mosaic_array(
    ds_arr: np.ndarray,
    mos_arr: np.ndarray,
    mask_subset: np.ndarray,
    indices: Tuple[int, ...],
) -> None:
    """
    Update the mosaic array with the given dataset

    :param ds_arr: Dataset ndarray
    :param mos_arr: Mosaic ndarray
    :param mask_subset: Pixel selection mask
    :param indices: Dataset and Mosaic indices to select the overlap
    """
    (dy_min, dy_max, dx_min, dx_max, my_min, my_max, mx_min, mx_max) = indices
    ds_subset = ds_arr[:, dy_min:dy_max, dx_min:dx_max]
    mos_subset = mos_arr[:, my_min:my_max, mx_min:mx_max]
    for i in range(0, ds_arr.shape[0]):
        mos_arr[i, my_min:my_max, mx_min:mx_max] = np.where(
            mask_subset, ds_subset[i, :, :], mos_subset[i, :, :]
        )
