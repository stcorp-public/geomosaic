"""
Mosaic runner for Landsat SR products
"""

import datetime as dt
import pathlib
from typing import Dict, Tuple, Any
from osgeo import gdal  # type: ignore
from osgeo import ogr  # type: ignore
import numpy as np  # type: ignore

from .util import calculate_slice_indices, get_roi_bbox
from .runner import Runner, update_mosaic_array

__author__ = "Janis Gailis"
__email__ = "gailis@stcorp.no"

# Enable ogr and gdal exceptions
gdal.UseExceptions()
ogr.UseExceptions()


PX_SZ = 30  # EPSG projection units (meter)


class RunnerLandsat(Runner):
    """Base runner class for Landsat Surface Reflectance products"""

    @staticmethod
    def date_key(filename: str) -> dt.datetime:
        """
        Extract date from the given filepath for sorting

        :param filepath: '/path/to/Lxxx_SCxx.tif'
        """
        filepath = pathlib.Path(filename)
        return dt.datetime.strptime(filepath.name[10:18], "%Y%m%d")

    @staticmethod
    def get_repr_filename(filename: str, utm_zone: int) -> str:
        """
        Get reprojected file filename.

        Goes from:
        Lxxxx-SCxxx.tif

        to:
        Lxxxx-SCxxx-UTMzone.tif

        :param filename: Filename of original file
        :param utm_zone: UTM zone
        """
        return f'{filename[:-len(".tif")]}-UTM{utm_zone}.tif'

    def initialize_outputs(self) -> None:
        """Initialize outputs"""
        bbox = get_roi_bbox(self.roi, self.mos_srs)
        x_min, x_max, y_min, y_max = self.pad_bbox(bbox, PX_SZ)

        # Initialize the mosaics
        print("Initialize the mosaic")
        driver = gdal.GetDriverByName("GTiff")
        if driver is None:
            raise ValueError("Can't find GeoTiff driver")

        first_ds = gdal.Open(self.input_dsets[0])

        tmap_path = self.outfilepath.parent / (
            self.outfilepath.stem + "_tilemap.tif"
        )

        xsize = int((x_max - x_min) // PX_SZ)
        ysize = int((y_max - y_min) // PX_SZ)

        # If memory becomes an issue, try using np.memmap here
        # For L8 and L4-7 pixel_qa fill value is 1
        mos_arr = np.ones([first_ds.RasterCount, ysize, xsize], dtype="int16")
        tmap_arr = np.zeros([1, ysize, xsize], dtype="uint16")
        self.score_arr = np.zeros([ysize, xsize], dtype="float16")

        # Note that objects of type np.ndarray and dict are mutable, everything
        # else here is not
        mos_geot = (x_min, PX_SZ, 0, y_max, 0, -PX_SZ)
        tmap_metadata: Dict[str, str] = {}
        self.outputs = {
            "mos": {
                "path": self.outfilepath,
                "array": mos_arr,
                "geot": mos_geot,
                "proj": self.mos_srs.ExportToWkt(),
                "meta": {},
            },
            "tmap": {
                "path": tmap_path,
                "array": tmap_arr,
                "geot": (x_min, PX_SZ, 0, y_max, 0, -PX_SZ),
                "proj": self.mos_srs.ExportToWkt(),
                "meta": tmap_metadata,
            },
        }

        # Close initialization dataset
        first_ds = None

    def initialize_dataset(self, path: pathlib.Path) -> Dict[str, Any]:
        """
        Initialize a dataset object holding a Landsat 8 or 4-7 SR dataset

        :param path: Path to GeoTIFF
        :return: Dataset object dict
        """
        product_uri = path.name[: -len(".tif")]
        ds_date = self.date_key(str(path))
        # Keys in 'dsets' must correspond to mosaic dataset keys in
        # self.outputs
        ds = {
            "uri": product_uri,
            "date": ds_date,
            "dsets": {
                "mos": {
                    "path": path,
                    "gdal": None,
                    "array": None,
                },
            },
        }

        return ds

    def update_mosaic(
        self, ds: Dict[str, Any], mask_subset: np.ndarray
    ) -> None:
        """
        Update the mosaic with the given dataset

        :param ds: Dataset object
        :param mask_subset: A mask with pixels that must be taken from this
        dataset
        """
        mos_geot = self.outputs["mos"]["geot"]
        mos_arr = self.outputs["mos"]["array"]
        ds_arr = ds["dsets"]["mos"]["array"]
        ds_gdal = ds["dsets"]["mos"]["gdal"]
        indices = calculate_slice_indices(
            mos_geot, ds_gdal.GetGeoTransform(), mos_arr.shape, ds_arr.shape
        )
        (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        ) = indices
        # Update the  mosaic
        indices = (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        )
        indices = np.array(indices, dtype=int)
        update_mosaic_array(ds_arr, mos_arr, mask_subset, indices)

    def reproject_dataset(
        self,
        source_srs: str,
        target_srs: str,
        pix_width: float,
        pix_height: float,
        ds_path: pathlib.Path,
        repr_path: pathlib.Path,
    ) -> gdal.Dataset:
        """
        Reproject the given dataset, apply a fix specific to Landsat datasets

        :param source_srs: Source reference system
        :param target_srs: Target reference system
        :param pix_width: Pixel width
        :param pix_height: Pixel height
        :param ds_path: Full path of the dataset to be reprojected
        :param repr_path: Full path of the reprojected dataset
        """
        ds = super().reproject_dataset(
            source_srs, target_srs, pix_width, pix_height, ds_path, repr_path
        )
        if ds is None:
            return None

        # Re-open in update mode
        ds = None
        ds = gdal.Open(str(repr_path), gdal.GA_Update)
        # Incorrectify geotransform to how Landsat8 files are expected to be,
        # shift the grid by half a pixel
        geot = list(ds.GetGeoTransform())
        geot[0] = geot[0] + pix_width // 2
        geot[3] = geot[3] - pix_height // 2
        ds.SetGeoTransform(geot)
        return ds

    @staticmethod
    def pad_bbox(
        bbox: Tuple[float, float, float, float], pixel_size: int
    ) -> Tuple[int, int, int, int]:
        """
        Pad a given bounding box, such that its extents fall on pixel
        boundaries, account for Landsat grid.

        :param bbox: [x_min, x_max, y_min, y_max]
        :param pixel_size: Pixel size in coordinate system units
        :return: [x_min, x_max, y_min, y_max]
        """
        # Pad the bounding box such that the mosaic grid corresponds to
        # 30m pixels. This assumes a unit of 1 meter. The Landsat grid starts
        # at 15/-15, although that's not reflected in the geotransform properly
        x_min, x_max, y_min, y_max = bbox
        half_px = pixel_size // 2
        x_min = x_min - (x_min % pixel_size) + half_px
        x_max = x_max + pixel_size - (x_max % pixel_size) + half_px
        y_min = y_min - (y_min % pixel_size) + half_px
        y_max = y_max + pixel_size - (y_max % pixel_size) + half_px

        return (int(x_min), int(x_max), int(y_min), int(y_max))
