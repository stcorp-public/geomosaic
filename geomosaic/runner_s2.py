"""
Mosaic runner for Sentinel-2
"""

import math
import datetime as dt
import pathlib
from typing import Dict, Tuple, Optional, Any
from osgeo import gdal  # type: ignore
from osgeo import ogr  # type: ignore
import numpy as np  # type: ignore
from numba import jit  # type: ignore

from .util import calculate_slice_indices, get_roi_bbox, pad_mask
from .runner import Runner, update_mosaic_array

__author__ = "Janis Gailis"
__email__ = "gailis@stcorp.no"

# Enable ogr and gdal exceptions
gdal.UseExceptions()
ogr.UseExceptions()


PX_SZ_20m = 20  # EPSG projection units (meter)
PX_SZ_10m = 10  # EPSG projection units (meter)


class RunnerS2(Runner):
    """Sentinel-2 mosaicing runner"""

    @property
    def band_indices(self) -> Dict[str, int]:
        return super().band_indices

    @band_indices.setter
    def band_indices(self, band_indices: Optional[Tuple[int, int]]) -> None:
        """
        :param band_indices: [SCL, AOT]
        """
        if band_indices is not None:
            self._band_indices = {
                "SCL": band_indices[0],
                "AOT": band_indices[1],
            }
        else:
            raise ValueError("Band indices needed")

    @staticmethod
    def date_key(filename: str) -> dt.datetime:
        """
        Extract date from the given filepath for sorting

        :param filepath: '/path/to/ST_L2A.tif'
        """
        filepath = pathlib.Path(filename)
        return dt.datetime.strptime(
            filepath.name.split("_")[2], "%Y%m%dT%H%M%S"
        )

    @staticmethod
    def get_repr_filename(filename: str, utm_zone: int) -> str:
        """
        Get reprojected file filename.

        Goes from:
        S2ID_<optional_tags>_resXX.tif

        to:
        S2ID_<optional_tags>_UTMzone_resXX.tif

        :param filename: Filename of original file
        :param utm_zone: UTM zone
        """
        repr_fn_list = filename.split("_")
        repr_fn_list.insert(-1, f"UTM{utm_zone}")
        repr_filename = "_".join(repr_fn_list)
        return repr_filename

    @staticmethod
    def is_full_dataset(fauxname: str) -> bool:
        """
        Make sure the given 'path/S2id' faux-filename has all expected
        resolution files (_10m.tif, _20m.tif) present. If not, log a warning

        :param fauxname: 'path/to/S2id'
        """
        path = pathlib.Path(fauxname).resolve()

        resolutions = ["_10m.tif", "_20m.tif"]

        for res in resolutions:
            filepath = path.parent / (path.stem + res)
            if not filepath.is_file():
                print(
                    f"The given dataset: {path.stem}, does not have "
                    f"the {filepath.name} file. Dataset will "
                    "not be processed."
                )
                return False

        return True

    def calculate_desirability_score(
        self, ds_arr: np.ndarray, ds_date: dt.datetime
    ) -> float:
        """
        Calculate the 'desirability' score for the given dataset. This is done
        by doing a trade-off between how close the dataset is to the target
        date, what's the average AOT, and how many good pixels there are,
        according to given weights

        :param ds_arr: ndarray containing the dataset
        :param ds_date: Dataset date
        :return: Floating point number between 0 and 1, 1 is most desirable
        """
        target_date = self.target_date
        band_indices = self.band_indices
        weights = self.weights

        aot_band = ds_arr[band_indices["AOT"] - 1, :, :]

        # AOT values are 0..1000 representing a range of 0..1, where 1 is worst
        aot_score = 1 - aot_band.mean() * 0.001

        # Draw a 0..1 value from a unit Gaussian depending on some defined
        # desired good distance to the target date
        date_diff = target_date - ds_date
        date_score = math.exp(
            -(math.pow(date_diff.days / self.date_stddev, 2))
        )

        # 4-vegetation, 5-bare soil, 6-water, 7-unclassified
        scl = ds_arr[band_indices["SCL"] - 1, :, :]
        gpix_score = np.sum((scl >= 4) & (scl < 7)) / (
            ds_arr.shape[1] * ds_arr.shape[2]
        )
        weighed_score = (
            aot_score * weights["aot"] * 0.01
            + date_score * weights["target_date"] * 0.01
            + gpix_score * weights["n_pixels"] * 0.01
        )

        # Discriminate against reprojected datasets
        if ds_arr.shape[1] != ds_arr.shape[2]:
            weighed_score = weighed_score * 0.7

        print(f"Desirability score: {weighed_score}")

        return weighed_score

    def initialize_outputs(self) -> None:
        """Initialize outputs"""
        bbox = get_roi_bbox(self.roi, self.mos_srs)
        x_min, x_max, y_min, y_max = self.pad_bbox(bbox, PX_SZ_20m)

        # Initialize the mosaics
        print("Initialize the mosaic")
        driver = gdal.GetDriverByName("GTiff")
        if driver is None:
            raise ValueError("Can't find GeoTiff driver")

        S2idpath = pathlib.Path(self.input_dsets[0]).resolve()
        ds_20m_path = S2idpath.parent / (S2idpath.stem + "_20m.tif")
        ds_10m_path = S2idpath.parent / (S2idpath.stem + "_10m.tif")

        first_ds_20m = gdal.Open(str(ds_20m_path))
        first_ds_10m = gdal.Open(str(ds_10m_path))

        outpath_20m = self.outfilepath.parent / (
            self.outfilepath.stem + "_20m.tif"
        )
        outpath_10m = self.outfilepath.parent / (
            self.outfilepath.stem + "_10m.tif"
        )
        tmap_path = self.outfilepath.parent / (
            self.outfilepath.stem + "_tilemap.tif"
        )

        # 20m
        xsize = int((x_max - x_min) // PX_SZ_20m)
        ysize = int((y_max - y_min) // PX_SZ_20m)

        # If memory becomes an issue, try using np.memmap here
        mos_20m_arr = np.zeros(
            [first_ds_20m.RasterCount, ysize, xsize], dtype="uint16"
        )
        tmap_arr = np.zeros([1, ysize, xsize], dtype="uint16")
        self.score_arr = np.zeros([ysize, xsize], dtype="float16")
        xsize = int((x_max - x_min) // PX_SZ_10m)
        ysize = int((y_max - y_min) // PX_SZ_10m)
        mos_10m_arr = np.zeros(
            [first_ds_10m.RasterCount, ysize, xsize], dtype="uint16"
        )

        # Note that objects of type np.ndarray and dict are mutable, everything
        # else here is not
        mos_geot = (x_min, PX_SZ_20m, 0, y_max, 0, -PX_SZ_20m)
        tmap_metadata: Dict[str, str] = {}
        self.outputs = {
            "20m": {
                "path": outpath_20m,
                "array": mos_20m_arr,
                "geot": mos_geot,
                "proj": self.mos_srs.ExportToWkt(),
                "meta": {},
            },
            "10m": {
                "path": outpath_10m,
                "array": mos_10m_arr,
                "geot": (x_min, PX_SZ_10m, 0, y_max, 0, -PX_SZ_10m),
                "proj": self.mos_srs.ExportToWkt(),
                "meta": {},
            },
            "tmap": {
                "path": tmap_path,
                "array": tmap_arr,
                "geot": (x_min, PX_SZ_20m, 0, y_max, 0, -PX_SZ_20m),
                "proj": self.mos_srs.ExportToWkt(),
                "meta": tmap_metadata,
            },
        }

        # Close initialization datasets
        first_ds_20m = None
        first_ds_10m = None

    def initialize_dataset(self, path: pathlib.Path) -> Dict[str, Any]:
        """
        Initialize a dataset object holding all GDAL datasets that belong to a
        particular S2 dataset

        :param path: path/to/S2id
        :return: Dictionary containing the full dataset with some meta info
        """
        product_uri = path.stem
        ds_date = self.date_key(str(path))
        name_10m = f"{product_uri}_10m.tif"
        name_20m = f"{product_uri}_20m.tif"
        # Keys in 'dsets' must correspond to mosaic dataset keys in
        # self.outputs
        ds = {
            "uri": product_uri,
            "date": ds_date,
            "dsets": {
                "20m": {
                    "path": path.parent / name_20m,
                    "gdal": None,
                    "array": None,
                },
                "10m": {
                    "path": path.parent / name_10m,
                    "gdal": None,
                    "array": None,
                },
            },
        }

        return ds

    def construct_replacement_mask(
        self, ds: Dict[str, Any]
    ) -> Tuple[np.ndarray, Tuple[int, ...]]:
        """
        Construct the pixel replacement mask.

        :param ds: A dataset object
        :return: [mask, tmap_replacement_indices]
        """
        # Retrieves the mosaic currently being constructed
        mos_20m_arr = self.outputs["20m"]["array"]
        mos_geot = self.outputs["20m"]["geot"]
        band_indices = self.band_indices
        ds_date = ds["date"]
        ds_20m = ds["dsets"]["20m"]["gdal"]
        # Construct the pixel replacement mask
        mos_scl = mos_20m_arr[band_indices["SCL"] - 1, :, :]
        ds["dsets"]["20m"]["array"] = ds_20m.ReadAsArray()
        ds_arr = ds["dsets"]["20m"]["array"]
        # Dataset classes
        ds_scl = ds_arr[band_indices["SCL"] - 1, :, :]

        score = self.calculate_desirability_score(ds_arr, ds_date)

        indices = calculate_slice_indices(
            mos_geot, ds_20m.GetGeoTransform(), mos_20m_arr.shape, ds_arr.shape
        )
        (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        ) = indices

        ds_scl = ds_scl[dy_min:dy_max, dx_min:dx_max]
        mos_scl = mos_scl[my_min:my_max, mx_min:mx_max]

        # Pads medium prob, high prob, thin cirrus, and snow in the dataset
        # to avoid adding edges of these effects to the mosaic and potentially
        # exchanging existing good pixels
        ds_clouds = pad_mask(
            ((ds_scl > 7) & (ds_scl < 11)), self.cloud_mask_padding_px
        )

        # Pad the mosaic cloud mask to ensure cloud edges would be replaced
        # by actual good pixels
        mos_clouds = pad_mask(
            ((mos_scl == 8) | (mos_scl == 9)), self.cloud_mask_padding_px
        )
        mos_cirrus = pad_mask((mos_scl == 10), self.cloud_mask_padding_px)
        # veg, bare, water
        ds_gpix = (ds_scl >= 4) & (ds_scl < 7) & (np.logical_not(ds_clouds))
        # good pixels + dark area, cloud shadow
        ds_gpix_dark = (
            (ds_scl >= 2) & (ds_scl <= 7) & (np.logical_not(ds_clouds))
        )

        # We select pixels that are of some class in the mosaic, and then
        # replace those with overlapping, progressively better, pixels

        # no data & saturated or defective => everything else
        mask_subset = (mos_scl < 2) & (ds_scl >= 2)
        # cloud medium prob & cloud high prob => everything except no data,
        # defective and clouds
        mask_subset = mask_subset | (
            mos_clouds & (ds_gpix_dark | (ds_scl >= 10))
        )
        # thin cirrus => good pixels & dark & snow/ice
        mask_subset = mask_subset | (
            mos_cirrus & (ds_gpix_dark | (ds_scl == 11))
        )
        # snow/ice => good pixels & dark
        mask_subset = mask_subset | ((mos_scl == 11) & ds_gpix_dark)
        # dark, shadow => good pixels
        mask_subset = mask_subset | (
            ((mos_scl == 2) | (mos_scl == 3)) & ds_gpix
        )
        # unclassified => good pixels
        mask_subset = mask_subset | ((mos_scl == 7) & ds_gpix)
        # good pixels => good pixels only if current score for the given pixel
        # is better than previous best
        maybe_subset = mask_subset | (
            ((mos_scl >= 4) & (mos_scl <= 7))
            & ds_gpix
            & np.logical_not(mos_clouds)
            & np.logical_not(mos_cirrus)
        )
        score_subset = self.score_arr[my_min:my_max, mx_min:mx_max]
        mask_subset = np.where(score_subset < score, maybe_subset, mask_subset)

        # Update the desirability score array
        self.score_arr[my_min:my_max, mx_min:mx_max] = np.where(
            score > score_subset, score, score_subset
        )

        mos_scl = None
        ds_scl = None

        return (mask_subset, indices)

    def should_add(self, ds_name: str) -> bool:
        """
        Determine if the given dataset should be added to the mosaic, if not,
        log a warning

        :param ds_name: Dataset '/path/to/S2id'
        :return: True or False
        """
        # For Sentinel 2, a dataset name is not an actual filename,
        # as each dataset consists of multiple resolution files
        filename_20m = ds_name + "_20m.tif"
        return super().should_add(filename_20m)

    def update_mosaic(
        self, ds: Dict[str, Any], mask_subset: np.ndarray
    ) -> None:
        """
        Update the mosaic with the given dataset

        :param ds: Dataset object
        :param mask_subset: Pixel selection mask
        """
        mos_geot = self.outputs["20m"]["geot"]
        ds_20m = ds["dsets"]["20m"]["gdal"]
        ds["dsets"]["20m"]["array"] = ds_20m.ReadAsArray()
        ds_arr = ds["dsets"]["20m"]["array"]
        mos_20m_arr = self.outputs["20m"]["array"]
        mos_10m_arr = self.outputs["10m"]["array"]

        indices = calculate_slice_indices(
            mos_geot, ds_20m.GetGeoTransform(), mos_20m_arr.shape, ds_arr.shape
        )
        (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        ) = indices
        # Update the 20m  mosaic
        indices = (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        )
        indices = np.array(indices, dtype=int)
        update_mosaic_array(ds_arr, mos_20m_arr, mask_subset, indices)
        ds_arr = None

        # Update the 10m mosaic
        # Construct indices,  simple multiplication can fail with reprojected
        # datasets
        full_20m_mask = np.zeros(
            (mos_20m_arr.shape[1], mos_20m_arr.shape[2]), dtype=bool
        )
        full_20m_mask[my_min:my_max, mx_min:mx_max] = mask_subset
        ds_10m = ds["dsets"]["10m"]["gdal"]
        ds["dsets"]["10m"]["array"] = ds_10m.ReadAsArray()
        ds_arr = ds["dsets"]["10m"]["array"]
        mos_geot = self.outputs["10m"]["geot"]
        indices = calculate_slice_indices(
            mos_geot, ds_10m.GetGeoTransform(), mos_10m_arr.shape, ds_arr.shape
        )
        indices = np.array(indices, dtype=int)
        update_10m_mosaic_array(ds_arr, mos_10m_arr, full_20m_mask, indices)

        return


@jit(nopython=True, parallel=True)
def update_10m_mosaic_array(
    ds_arr: np.ndarray,
    mos_arr: np.ndarray,
    full_20m_mask: np.ndarray,
    indices: Tuple[int, ...],
) -> None:
    """
    Update the 10m mosaic

    :param ds_arr: Dataset ndarray
    :param mos_arr: Mosaic ndarray
    :param full_20m_mask: Pixel selection mask at 20m resolution
    :param indices: Dataset and Mosaic indices to select the overlap
    """
    (dy_min, dy_max, dx_min, dx_max, my_min, my_max, mx_min, mx_max) = indices
    # Upsample the mask
    mask_10m = np.kron(full_20m_mask, np.ones((2, 2)))
    mask_subset = mask_10m[my_min:my_max, mx_min:mx_max]
    update_mosaic_array(ds_arr, mos_arr, mask_subset, indices)
