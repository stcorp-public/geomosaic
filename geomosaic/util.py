"""
Utilities for geomosaic
"""
import sys
import os
import errno
import io
import subprocess
from typing import Tuple, Optional
from osgeo import ogr  # type: ignore
from osgeo import osr  # type: ignore
from osgeo import gdal  # type: ignore
from toolz import curry  # type: ignore
import click
import datetime as dt
import pathlib

import numpy as np
from scipy.ndimage import binary_dilation, generate_binary_structure


def validate_execution_env() -> None:
    """
    Validate the execution environment
    """
    # Check Python version
    if sys.version_info.major < 3 and sys.version_info.minor < 6:
        # Due to pathlib non-strict features
        print("Python >= 3.6 required")
        sys.exit(1)

    # make sure required binary dependencies are met
    try:
        with open(os.devnull, "w") as f:
            subprocess.call(["gdalwarp", "--help"], stdout=f)
    except OSError as e:
        if e.errno == errno.ENOENT:
            print("gdalwarp must be installed to run this script.")
            sys.exit(1)
        else:
            raise


def seconds_to_hr(total: float) -> Tuple[float, float, float, float]:
    """
    Convert seconds to human readable time

    :param total: Total time in seconds
    :return: (days, hours, minutes, seconds)
    """
    days = total // 86400
    hours = total // 3600 % 24
    minutes = total // 60 % 60
    seconds = total % 60
    return (days, hours, minutes, seconds)


def get_ds_envelope(ds: gdal.Dataset) -> ogr.Geometry:
    """
    Get the envelope of the given GDAL dataset
    :param ds: GDAL dataset to inspect
    :return: Envelope of the given dataset as an osgeo.ogr.Geometry
    """
    ds_geot = ds.GetGeoTransform()
    x_min = ds_geot[0]
    y_max = ds_geot[3]
    x_max = x_min + (ds_geot[1] * ds.RasterXSize)
    y_min = y_max + (ds_geot[5] * ds.RasterYSize)

    ds_ref = osr.SpatialReference()
    ds_ref.ImportFromWkt(ds.GetProjection())
    roi_srs = osr.SpatialReference()
    roi_srs.SetWellKnownGeogCS("WGS84")
    srs_to_deg = osr.CoordinateTransformation(ds_ref, roi_srs)

    tl = srs_to_deg.TransformPoint(x_min, y_max)
    tr = srs_to_deg.TransformPoint(x_max, y_max)
    bl = srs_to_deg.TransformPoint(x_min, y_min)
    br = srs_to_deg.TransformPoint(x_max, y_min)
    ds_env_wkt = (
        f"POLYGON (({tl[0]} {tl[1]},{tr[0]} {tr[1]},"
        f"{br[0]} {br[1]},{bl[0]} {bl[1]}))"
    )
    ds_env = ogr.CreateGeometryFromWkt(ds_env_wkt, reference=roi_srs)
    ds_env.CloseRings()
    return ds_env


@curry
def get_index(
    src_geot: Tuple[float, ...],
    dst_geot: Tuple[float, ...],
    axis: str,
    index: int,
) -> int:
    """
    Express an index described by the source GDAL Geontransform
    as an index described by destination GDAL Geotransform. The provided
    index can be larger than the maximum axis size.

    This assumes the given Geotransforms have the same projection

    :param ds_src: Source dataset
    :param ds_dst: Destination dataset
    :param axis: 'X' or 'Y'
    :param index: Source dataset index to convert
    :return: Converted index
    """
    diff = 0
    if src_geot[1] != dst_geot[1] or src_geot[5] != dst_geot[5]:
        raise ValueError("Pixel sizes differ between geotransforms")

    if axis == "X":
        diff = int((src_geot[0] - dst_geot[0]) // src_geot[1])
    elif axis == "Y":
        diff = int((src_geot[3] - dst_geot[3]) // src_geot[5])
    else:
        raise ValueError("Bad axis value.")

    new_index = index + diff

    if new_index < 0:
        return int(0)

    return int(new_index)


def calculate_slice_indices(
    mos_geot: Tuple[float, ...],
    ds_geot: Tuple[float, ...],
    mos_shape: Tuple[int, int, int],
    ds_shape: Tuple[int, int, int],
) -> Tuple[int, ...]:
    """
    Calculate slicing indices that would produce identically sized slices
    of the overlap between the mosaic and the dataset

    :param mos_geot: Mosaic GDAL geotransform
    :param ds_geot: Dataset GDAL geotransform
    :param mos_shape: Mosaic 3D array shape
    :param ds_shape: Dataset 3D array shape
    :return: [dy_min, dy_max, dx_min, dx_max, my_min, my_max, mx_min, mx_max]
    """
    get_mos_x_ind = get_index(ds_geot, mos_geot, "X")
    get_mos_y_ind = get_index(ds_geot, mos_geot, "Y")
    get_ds_x_ind = get_index(mos_geot, ds_geot, "X")
    get_ds_y_ind = get_index(mos_geot, ds_geot, "Y")

    mx_min, mx_max = [x for x in map(get_mos_x_ind, [0, ds_shape[2] - 1])]
    my_min, my_max = [x for x in map(get_mos_y_ind, [0, ds_shape[1] - 1])]
    dx_min, dx_max = [x for x in map(get_ds_x_ind, [0, mos_shape[2] - 1])]
    dy_min, dy_max = [x for x in map(get_ds_y_ind, [0, mos_shape[1] - 1])]

    mx_max, my_max, dx_max, dy_max = [
        x for x in map(lambda x: x + 1, [mx_max, my_max, dx_max, dy_max])
    ]

    # Max values can be larger than extents
    mos_ysize = mos_shape[1]
    mos_xsize = mos_shape[2]
    ds_ysize = ds_shape[1]
    ds_xsize = ds_shape[2]
    mx_max = mx_max if mx_max <= mos_xsize else mos_xsize
    my_max = my_max if my_max <= mos_ysize else mos_ysize
    dx_max = dx_max if dx_max <= ds_xsize else ds_xsize
    dy_max = dy_max if dy_max <= ds_ysize else ds_ysize

    return (dy_min, dy_max, dx_min, dx_max, my_min, my_max, mx_min, mx_max)


def validate_weights(
    ctx: click.core.Context,
    param: click.core.Option,
    weight: Tuple[int, int, int],
) -> Tuple[int, int, int]:
    """Validator callback for weights parameter"""
    if sum(weight) != 100:
        raise click.BadParameter("The sum of all weights must be 100")
    return weight


def validate_wkt(
    ctx: click.core.Context,
    param: click.core.Option,
    wkt_file: io.TextIOWrapper,
) -> ogr.Geometry:
    """Validator callback for WKT region"""
    roi_wkt = wkt_file.read()

    # This works as a sanity validator too
    try:
        roi = ogr.CreateGeometryFromWkt(roi_wkt)
        pass
    except RuntimeError as e:
        raise click.BadParameter(str(e))

    return roi


def validate_date(
    ctx: click.core.Context, param: click.core.Option, date: Optional[str]
) -> Optional[dt.datetime]:
    """Validator callback for the target date parameter"""
    if date is None:
        return None

    try:
        return dt.datetime.strptime(date, "%Y%m%d")
    except ValueError:
        raise click.BadParameter("Target date must be in YYYYMMDD format.")
    return date


def get_full_outname(output: str) -> pathlib.Path:
    """Create nested output dir if needed"""
    # Don't make this a callback. Otherwise directories might
    # get created even on runs that fail sanity checks
    outname = pathlib.Path(output)
    outname = outname.resolve()
    oldmask = os.umask(000)
    outname.parent.mkdir(parents=True, exist_ok=True, mode=0o777)
    os.umask(oldmask)
    return outname


def get_roi_bbox(
    roi: ogr.Geometry, target_srs: int
) -> Tuple[float, float, float, float]:
    """
    Get a bounding box of a region of interest in the given EPSG reference
    system.

    :param roi: A region of interest as a osgeo.ogr.Geometry
    :param mos_srs: Target EPSG reference system
    :return: [x_min, x_max, y_min, y_max]
    """
    # Get a bounding box of the ROI in the target EPSG reference system
    roi_srs = osr.SpatialReference()
    roi_srs.SetWellKnownGeogCS("WGS84")

    lon_min, lon_max, lat_min, lat_max = roi.GetEnvelope()
    deg_to_srs = osr.CoordinateTransformation(roi_srs, target_srs)
    top_left = deg_to_srs.TransformPoint(lon_min, lat_max)
    top_right = deg_to_srs.TransformPoint(lon_max, lat_max)
    bottom_left = deg_to_srs.TransformPoint(lon_min, lat_min)
    bottom_right = deg_to_srs.TransformPoint(lon_max, lat_min)

    # A 'rectangular' envelope in lat/lon is probably not rectangular in
    # some EPSG reference system
    x_min = min(top_left[0], bottom_left[0])
    x_max = max(top_right[0], bottom_right[0])
    y_min = min(bottom_left[1], bottom_right[1])
    y_max = max(top_left[1], top_right[1])

    return (x_min, x_max, y_min, y_max)


def pad_mask(mask: np.ndarray, clmp_px: int):
    """
    Pads the cloud mask by expanding it in all directions with a
    given number of pixels.

    :param mask: Mask array
    :param clmp_px: Number of pixels to pad.
    :return: Padded mask array
    """
    if clmp_px == 0:
        return mask
    structure2 = generate_binary_structure(2, 2)
    mask = binary_dilation(mask, structure2, iterations=clmp_px).astype(
        mask.dtype
    )
    return mask
