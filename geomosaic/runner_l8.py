"""
Mosaic runner for Landsat 8 SR datasets. Based on Landsat 8 Collection 1 (C1)
Land Surface Reflectance Code (LaSRC) Product Guide V3.0, dated August 2020.
"""

import math
import datetime as dt
from typing import Dict, Tuple, Optional, Any
from osgeo import gdal  # type: ignore
from osgeo import ogr  # type: ignore
import numpy as np  # type: ignore

from .util import calculate_slice_indices, pad_mask
from .runner_landsat import RunnerLandsat

__author__ = "Janis Gailis"
__email__ = "gailis@stcorp.no"

# Enable ogr and gdal exceptions
gdal.UseExceptions()
ogr.UseExceptions()


class RunnerL8(RunnerLandsat):
    """Landsat 8 mosaicing runner"""

    @property
    def band_indices(self) -> Dict[str, int]:
        return super().band_indices

    @band_indices.setter
    def band_indices(self, band_indices: Optional[Tuple[int, int]]) -> None:
        """
        :param band_indices: [pixel_qa, sr_aerosol]
        """
        if band_indices is not None:
            self._band_indices = {
                "pixel_qa": band_indices[0],
                "sr_aerosol": band_indices[1],
            }
        else:
            raise ValueError("Band indices needed")

    def calculate_desirability_score(
        self, ds_arr: np.ndarray, ds_date: dt.datetime
    ) -> float:
        """
        Calculate the 'desirability' score for the given dataset. This is done
        by doing a trade-off between how close the dataset is to the target
        date, what's the average AOT, and how many good pixels there are,
        according to given weights

        :param ds_arr: ndarray containing the dataset
        :param ds_date: Dataset date
        :return: Floating point number between 0 and 1, 1 is most desirable
        """
        target_date = self.target_date
        band_indices = self.band_indices
        weights = self.weights

        aot_band = ds_arr[band_indices["sr_aerosol"] - 1, :, :]

        # sr_aerosol has bit encoded values where 0b00xxxxxx means
        # climatology level aerosol and 0b01xxxxxx means low aerosol
        # level. 0b10x.. is medium and 0b11x.. is high. We try to maximize
        # low aerosol and clear pixels.
        aot_clim = np.sum((aot_band < 64) & (aot_band > 1))
        aot_low = np.sum(np.bitwise_and(aot_band, 0b01000000) == 0b01000000)
        aot_score = (aot_clim + aot_low) / (ds_arr.shape[1] * ds_arr.shape[2])

        # Draw a 0..1 value from a unit Gaussian depending on some defined
        # desired good distance to the target date
        date_diff = target_date - ds_date
        date_score = math.exp(
            -(math.pow(date_diff.days / self.date_stddev, 2))
        )

        # 322 - clear terrain, low cloud, no occlusion
        # 324 - water, low cloud, no occlusion
        scl = ds_arr[band_indices["pixel_qa"] - 1, :, :]
        gpix_score = np.sum((scl == 322) | (scl == 324)) / (
            ds_arr.shape[1] * ds_arr.shape[2]
        )
        weighed_score = (
            aot_score * weights["aot"] * 0.01
            + date_score * weights["target_date"] * 0.01
            + gpix_score * weights["n_pixels"] * 0.01
        )

        print(f"Desirability score: {weighed_score}")

        return weighed_score

    def construct_replacement_mask(
        self, ds: Dict[str, Any]
    ) -> Tuple[np.ndarray, Tuple[int, ...]]:
        """
        Construct the pixel replacement mask

        :param ds: A dataset object
        :return: [mask, tmap_replacement_indices]
        """
        mos_arr = self.outputs["mos"]["array"]
        mos_geot = self.outputs["mos"]["geot"]
        band_indices = self.band_indices
        ds_date = ds["date"]
        ds_gdal = ds["dsets"]["mos"]["gdal"]
        ds["dsets"]["mos"]["array"] = ds["dsets"]["mos"]["gdal"].ReadAsArray()
        ds_arr = ds["dsets"]["mos"]["array"]
        # Construct the pixel replacement mask
        mos_scl = mos_arr[band_indices["pixel_qa"] - 1, :, :]
        ds_scl = np.array(
            ds_arr[band_indices["pixel_qa"] - 1, :, :], dtype="uint16"
        )

        score = self.calculate_desirability_score(ds_arr, ds_date)

        indices = calculate_slice_indices(
            mos_geot, ds_gdal.GetGeoTransform(), mos_arr.shape, ds_arr.shape
        )
        (
            dy_min,
            dy_max,
            dx_min,
            dx_max,
            my_min,
            my_max,
            mx_min,
            mx_max,
        ) = indices

        ds_scl = ds_scl[dy_min:dy_max, dx_min:dx_max]
        mos_scl = mos_scl[my_min:my_max, mx_min:mx_max]

        # Cloud medium prob or cloud high prob in the dataset are padded to
        # avoid overwriting actual good pixels that may be in the mosaic
        # already with cloud edges mislabeled as good pixels
        ds_cld = np.bitwise_and(ds_scl, 128)
        ds_cld = pad_mask((ds_cld == 128), self.cloud_mask_padding_px)

        # Pad the clouds already in the mosaic to make sure we exchange
        # cloud edges with better pixels if possible
        mos_cld = np.bitwise_and(mos_scl, 128)
        mos_cld = pad_mask((mos_cld == 128), self.cloud_mask_padding_px)

        # clear terrain, low cloud & water, low cloud
        ds_gpix = ((ds_scl == 322) | (ds_scl == 324)) & (
            np.logical_not(ds_cld)
        )
        # good pixels + dark area, cloud shadow, low cloud
        ds_gpix_dark = ds_gpix | (ds_scl == 328)
        # good pixels + dark + snow/ice
        ds_gpix_dark_snow = (ds_scl == 336) | ds_gpix_dark

        # no data & saturated or defective => everything else
        mask_subset = ((mos_scl == 1) | (mos_scl == 0)) & (
            (ds_scl != 1) | ds_scl != 0
        )

        # terrain occlusion to everything else
        mos_occ = np.bitwise_and(mos_scl, 1024)
        mos_occ = mos_occ == 1024
        ds_occ = np.bitwise_and(ds_scl, 1024)
        ds_not_occ = ds_occ != 1024
        mask_subset = mask_subset | (mos_occ & ds_not_occ)
        # cloud medium prob or cloud high prob => everything except no data,
        # defective and clouds
        mask_subset = mask_subset | (mos_cld & ds_gpix_dark_snow)
        # cirrus medium, high => good pixels & dark & snow/ice
        mos_crr = np.bitwise_and(mos_scl, 512)
        mos_crr = mos_crr == 512
        mask_subset = mask_subset | (mos_crr & ds_gpix_dark_snow)
        # snow/ice => good pixels & dark
        mos_snw = np.bitwise_and(mos_scl, 16)
        mos_snw = mos_snw == 16
        mask_subset = mask_subset | (mos_snw & ds_gpix_dark)
        # dark, shadow => good pixels
        mos_drk = np.bitwise_and(mos_scl, 8)
        mos_drk = mos_drk == 8
        mask_subset = mask_subset | (mos_drk & ds_gpix)
        # good pixels => good pixels only if current score for the given pixel
        # is better than previous best
        mos_gpix = ((mos_scl == 322) | (mos_scl == 324)) & (
            np.logical_not(mos_cld)
        )
        maybe_subset = mask_subset | (mos_gpix & ds_gpix)
        score_subset = self.score_arr[my_min:my_max, mx_min:mx_max]
        mask_subset = np.where(score_subset < score, maybe_subset, mask_subset)
        mos_scl = None
        ds_scl = None

        return (mask_subset, indices)
