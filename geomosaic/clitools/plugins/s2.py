import time
import click
import datetime as dt
import pathlib

from typing import List, Tuple, Optional

from osgeo import ogr  # type: ignore

from geomosaic.runner_s2 import RunnerS2
from geomosaic.util import (
    seconds_to_hr,
    validate_execution_env,
    validate_weights,
    validate_wkt,
    validate_date,
    get_full_outname,
)

# Enable ogr exceptions
ogr.UseExceptions()


__author__ = "Janis Gailis"
__email__ = "gailis@stcorp.no"


def _validate_input_list(
    ctx: click.core.Context, param: click.core.Argument, in_list: List[str]
) -> List[str]:
    """Validator callback for the input files list"""
    # Remove resolution identifier and extension
    in_path_S2id = [x[: -len("_XXm.tif")] for x in in_list]

    # Make the list unique, but convert back, as we need it ordered later
    in_path_S2id = list(set(in_path_S2id))

    # Sort by date
    try:
        in_path_S2id = sorted(in_path_S2id, key=RunnerS2.date_key)
    except ValueError:
        raise click.BadParameter(
            "An input file name does not contain an S2" " identifier"
        )

    # Make sure that each given 'path/S2id' has all expected resolutions
    # present
    in_path_S2id = [x for x in in_path_S2id if RunnerS2.is_full_dataset(x)]

    if len(in_path_S2id) < 2:
        raise click.BadParameter("At least two datasets needed to continue")

    return in_path_S2id


@click.command()
@click.option(  # type: ignore
    "-d",
    "--date",
    type=str,
    callback=validate_date,
    help="Target date for the output mosaic YYYYMMDD, "
    "if not given, defaults to"
    " the midpoint of dates of the given input files",
)
@click.option(  # type: ignore
    "-w",
    "--weight",
    type=int,
    nargs=3,
    default=[50, 40, 10],
    callback=validate_weights,
    help="Weights for different scene goodness score "
    "indicators DATE N_GOOD_PIXELS AOT,"
    " must add to 100. Defaults to 50 40 10.",
)
@click.option(
    "-b",
    "--band",
    type=int,
    nargs=2,
    default=[8, 9],
    help="Band indices for SCL and AOT bands in the given"
    "_20m input geotiffs SCL_IND AOT_IND."
    " 1 based index.",
)
@click.option(
    "-o",
    "--output",
    type=str,
    default="mosaic.tif",
    help="Output filename. By default mosaic.tif resulting"
    " in three output files: mosaic_10m.tif, mosaic_20m.tif,"
    " mosaic_tilemap.tif",
)
@click.option(
    "-p",
    "--projection",
    type=int,
    default=32632,
    help="Target EPSG projection of the resulting mosaics."
    " For performance purposes should be the"
    " projection that most of the tiles already are"
    " in. Default is 32632.",
)
@click.option(
    "-m",
    "--cloud-mask-padding",
    type=int,
    default=0,
    help="Number of pixels to pad the cloud mask with.",
)
@click.option(  # type: ignore
    "-r",
    "--roi-file",
    callback=validate_wkt,
    type=click.File("r"),
    required=True,
    help="File containing the region of interest as a " "WKT geometry.",
)
@click.option(
    "--date-stddev",
    type=int,
    default=30,
    help="Date score Gaussian standard deviation."
    " determines the trade-off between more clear"
    " sky scenes versus temporal homogeneity of"
    " the resulting mosaic. Default is 30 (days).",
)
@click.option(
    "--repr-path",
    type=click.Path(exists=True),
    help="Path to a directory where reprojected files"
    " should be saved and/or found. If not given"
    " , reprojected files are saved in the "
    " 'reprojected' directory of the parent directory"
    " of a particular input file.",
)
@click.option(
    "--reproject/--no-reproject",
    default=True,
    help="If files on a different projection should be reprojected "
    "or skipped",
)
@click.argument(  # type: ignore
    "input_files",
    callback=_validate_input_list,
    type=click.Path(exists=True),
    nargs=-1,
)
def cli(
    date: Optional[dt.datetime],
    weight: Tuple[int],
    band: Tuple[int],
    output: str,
    projection: int,
    cloud_mask_padding: int,
    roi_file: ogr.Geometry,
    date_stddev: int,
    repr_path: str,
    reproject: bool,
    input_files: List[str],
) -> None:
    """
    Create a Sentinel-2 mosaic.

    Creates a mosaic by selecting the best pixels from a bunch of
    Sentinel-2 scenes in geotiff format. All given scenes should have 10m
    and 20m geotiffs. Each scene should be provided to the utility as
    a GeoTIFF input file in the following form: '/path/to/<S2id>_XXm.tif'
    Providing a single resolution file is enough, duplicate scenes are not
    processed.

    Computation is carried out in an iterative manner going over all
    given scenes sorted by datetime. At each step invalid and bad
    pixels are replaced by better pixels according to this class
    hieararchy:

    NaN < Cloud < Cirrus < Snow/Ice < Shadow < Other classes

    Pixels belonging to Other classes are replaced with pixels of
    Other classes from the current scene only if the current scene
    has a better 'desirability score' than the previous best.

    The 'desirability score' is determined by doing a tradeoff between
    how close the scene is to the desired target date, how many
    pixels of Other classes are in this scene and what's the average
    aerosol optical thickness for this scene. The tradeoff is made
    based on the given weights.

    geomosaic s2 [OPTIONS] -r ROI_FILE input1.tif input2.tif inputN.tif

    If input file paths are saved in a file, use cat:

    geomosaic s2 [OPTIONS] -r ROI_FILE $(cat input_file_paths)
    """
    print(f"Date stddev: {date_stddev}")
    validate_execution_env()
    start_time = time.time()

    # Has been converted in callback
    roi = roi_file
    outname = get_full_outname(output)

    repr_path_converted = None
    if repr_path:
        repr_path_converted = pathlib.Path(repr_path)

    runner = RunnerS2(
        input_files,
        date,
        weight,
        band,
        cloud_mask_padding,
        outname,
        roi,
        projection,
        date_stddev,
        repr_path_converted,
        reproject,
    )

    click.echo(f"Target date: {runner.target_date}")

    runner.process()

    end_time = time.time()
    days, hours, minutes, seconds = seconds_to_hr(end_time - start_time)
    click.echo(
        f"Mosaic made in {days} days, {hours} hours,"
        f" {minutes} minutes and {seconds} seconds."
    )
