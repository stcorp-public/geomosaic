#!/usr/bin/env python3
"""
CLI entry point for the geomosaic package

The dynamic plugin loading logic taken from click documentation
"""

import os
import click
import pathlib

from typing import Optional, List, Dict

plugin_folder = pathlib.Path(__file__).parent / "plugins"


class GTiffCLI(click.MultiCommand):
    def list_commands(
        self, ctx: click.core.Context  # type: ignore
    ) -> List[str]:
        rv = []
        for filename in os.listdir(plugin_folder):
            if filename.endswith(".py"):
                rv.append(filename[:-3])
        rv.sort()
        return rv

    def get_command(
        self, ctx: click.core.Context, name: str
    ) -> Optional[click.core.Command]:
        ns: Dict[str, click.core.Command] = {}
        fn = plugin_folder / f"{name}.py"
        with open(fn) as f:
            code = compile(f.read(), fn, "exec")
            eval(code, ns, ns)
        try:
            return ns["cli"]
        except KeyError:
            return None


@click.command(cls=GTiffCLI)
def cli() -> None:
    pass
