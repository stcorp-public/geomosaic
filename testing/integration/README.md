This is a rudimentary integration test suite. It requires that the neccessary
input files described in the READMEs of appropriate directories are present.
These can be acquired by the usual means of getting Landsat and Sentinel data,
and in case of Sentinel also running `safe2geotiff`.

`geomosaic` must be installed and on PATH.

Then from the current directory:

```shell
$ bash s2_run.sh
$ bash l8_run.sh
```
