#!/usr/bin/env bash

num_tests=4

# Test 1: L8 nominal, reproject
echo "Running Test 1 of $num_tests"
geomosaic l8 -r shapes/buskerud.wkt -o out/l8mb_t1.tif --date-stddev 30 $(cat inputs/l8_buskerud_test1)
if [ ! -f out/l8mb_t1.tif ] || [ ! -f out/l8mb_t1_tilemap.tif ]; then
    echo "Test 1 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 2: Use cached
echo "Running Test 2 of $num_tests"
geomosaic l8 -r shapes/buskerud.wkt -o out/l8mb_t2.tif $(cat inputs/l8_buskerud_test1)
if [ ! -f out/l8mb_t2.tif ] || [ ! -f out/l8mb_t2_tilemap.tif ]; then
    echo "Test 2 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 3: Different reprojected file directory
echo "Running Test 2 of $num_tests"
geomosaic l8 -r shapes/buskerud.wkt -o out/l8mb_t3.tif --repr-path reprojected/ $(cat inputs/l8_buskerud_test1)
if [ ! -f out/l8mb_t3.tif ] || [ ! -f out/l8mb_t3_tilemap.tif ]; then
    echo "Test 3 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test that outputs are equal
echo "Verifying checksums..."
sum1=$(sha1sum out/l8mb_t1.tif | cut -d' ' -f1)
sum2=$(sha1sum out/l8mb_t2.tif | cut -d' ' -f1)
sum3=$(sha1sum out/l8mb_t3.tif | cut -d' ' -f1)

if [ ! "$sum1" = "$sum2" ]; then
    echo "Failure. Output checksums are not equal for Tests 1 and 2 but should be."
    exit 1
fi

if [ ! "$sum1" = "$sum3" ]; then
    echo "Failure. Output checksums are not equal for Tests 1 and 3 but should be."
    exit 1
fi

# Test 4: Test --no-reproject flag
echo "Running Test 4 of $num_tests"
geomosaic l8 -r shapes/buskerud.wkt -o out/l8mb_t4.tif --no-reproject $(cat inputs/l8_buskerud_test1)
if [ ! -f out/l8mb_t4.tif ] || [ ! -f out/l8mb_t4_tilemap.tif ]; then
    echo "Test 4 failed, some of the outputs have not been created!!"
    exit 1
fi

test_sum4="98214a5a4d55831b8b8ced12adb42be84b77d060"
sum4=$(sha1sum out/l8mb_t4.tif | cut -d' ' -f1)

if [ ! "$test_sum4" = "$sum4" ]; then
    echo "Failure. Output checksum for Test 4 is not equal with the saved test checksum."
    exit 1
fi

echo "OK! (probably)"

# Clean up
rm -rf out
rm -rf input_files_l8/reprojected
rm reprojected/*.tif
