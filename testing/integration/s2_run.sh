#!/usr/bin/env bash

num_tests=8

# Test 1: Input list all _20m.tif
echo "Running Test 1 of $num_tests"
geomosaic s2 -r shapes/buskerud.wkt -o out/s2mb_t1.tif --date-stddev 30 $(cat inputs/s2_buskerud_test1)
if [ ! -f out/s2mb_t1_10m.tif ] || [ ! -f out/s2mb_t1_20m.tif ] || [ ! -f out/s2mb_t1_tilemap.tif ]; then
    echo "Test 1 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 2: Input list all _10m.tif, duplicate entries
echo "Running Test 2 of $num_tests"
geomosaic s2 -r shapes/buskerud.wkt -o out/s2mb_t2.tif $(cat inputs/s2_buskerud_test2)
if [ ! -f out/s2mb_t2_10m.tif ] || [ ! -f out/s2mb_t2_20m.tif ] || [ ! -f out/s2mb_t2_tilemap.tif ]; then
    echo "Test 2 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 3: Input list _10m.tif and _20m.tif
echo "Running Test 3 of $num_tests"
geomosaic s2 -r shapes/buskerud.wkt -o out/s2mb_t3.tif $(cat inputs/s2_buskerud_test3)
if [ ! -f out/s2mb_t3_10m.tif ] || [ ! -f out/s2mb_t3_20m.tif ] || [ ! -f out/s2mb_t3_tilemap.tif ]; then
    echo "Test 3 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test that outputs are equal
echo "Verifying checksums..."
sum1=$(sha1sum out/s2mb_t1_20m.tif | cut -d' ' -f1)
sum2=$(sha1sum out/s2mb_t2_20m.tif | cut -d' ' -f1)
sum3=$(sha1sum out/s2mb_t3_20m.tif | cut -d' ' -f1)

if [ ! "$sum1" = "$sum2" ] || [ ! "$sum1" = "$sum3" ]; then
    echo "Failure. Output checksums are not equal for Tests 1, 2 and 3 but should be."
    exit 1
fi

# Test 4: Reprojection
echo "Running Test 4 of $num_tests"
geomosaic s2 -r shapes/hordaland.wkt -o out/s2mh_t4.tif $(cat inputs/s2_hordaland_test4)
if [ ! -f out/s2mh_t4_10m.tif ] || [ ! -f out/s2mh_t4_20m.tif ] || [ ! -f out/s2mh_t4_tilemap.tif ]; then
    echo "Test 4 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 5: Using cached reprojection
echo "Running Test 5 of $num_tests"
geomosaic s2 -r shapes/hordaland.wkt -o out/s2mh_t5.tif $(cat inputs/s2_hordaland_test4)
if [ ! -f out/s2mh_t5_10m.tif ] || [ ! -f out/s2mh_t5_20m.tif ] || [ ! -f out/s2mh_t5_tilemap.tif ]; then
    echo "Test 5 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 6: Using reprojected files as input data
echo "Running Test 6 of $num_tests"
geomosaic s2 -r shapes/hordaland.wkt -o out/s2mh_t6.tif $(cat inputs/s2_hordaland_test6)
if [ ! -f out/s2mh_t6_10m.tif ] || [ ! -f out/s2mh_t6_20m.tif ] || [ ! -f out/s2mh_t6_tilemap.tif ]; then
    echo "Test 6 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test 7: Reprojection in a different directory
echo "Running Test 7 of $num_tests"
geomosaic s2 -r shapes/hordaland.wkt -o out/s2mh_t7.tif --repr-path reprojected/ $(cat inputs/s2_hordaland_test4)
if [ ! -f out/s2mh_t7_10m.tif ] || [ ! -f out/s2mh_t7_20m.tif ] || [ ! -f out/s2mh_t7_tilemap.tif ]; then
    echo "Test 7 failed, some of the outputs have not been created!!"
    exit 1
fi

# Test that outputs are equal
echo "Verifying checksums..."
sum4=$(sha1sum out/s2mh_t4_20m.tif | cut -d' ' -f1)
sum5=$(sha1sum out/s2mh_t5_20m.tif | cut -d' ' -f1)
sum6=$(sha1sum out/s2mh_t6_20m.tif | cut -d' ' -f1)
sum7=$(sha1sum out/s2mh_t7_20m.tif | cut -d' ' -f1)

if [ ! "$sum4" = "$sum5" ]; then
    echo "Failure. Output checksums are not equal for Tests 4 and 5 but should be."
    exit 1
fi

if [ ! "$sum4" = "$sum6" ]; then
    echo "Failure. Output checksums are not equal for Tests 4 and 6 but should be."
    exit 1
fi

if [ ! "$sum4" = "$sum7" ]; then
    echo "Failure. Output checksums are not equal for Tests 4 and 7 but should be."
    exit 1
fi

# Test 8: Test --no-reproject flag
echo "Running Test 8 of $num_tests"
geomosaic s2 -r shapes/hordaland.wkt -o out/s2mh_t8.tif --no-reproject $(cat inputs/s2_hordaland_test4)
if [ ! -f out/s2mh_t8_10m.tif ] || [ ! -f out/s2mh_t8_20m.tif ] || [ ! -f out/s2mh_t8_tilemap.tif ]; then
    echo "Test 8 failed, some of the outputs have not been created!!"
    exit 1
fi

test_sum8="5ee43dcc263c00f8f1c1f523732cd137940771f4"
sum8=$(sha1sum out/s2mh_t8_20m.tif | cut -d' ' -f1)

if [ ! "$test_sum8" = "$sum8" ]; then
    echo "Failure. Output checksum for Test 8 is not equal with the saved test checksum."
    exit 1
fi

echo "OK! (probably)"

# Clean up
rm -rf out
rm -rf input_files_s2/reprojected
rm reprojected/*
