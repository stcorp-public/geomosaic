import numpy as np
from geomosaic.util import pad_mask


def setup_dummy_data(num_x, num_y, num_mask_px):
    mask = np.zeros((num_x, num_y), dtype=bool)
    n_rnd_x = np.random.randint(0, num_x, num_mask_px)
    n_rnd_y = np.random.randint(0, num_y, num_mask_px)
    for ix, iy in zip(n_rnd_x, n_rnd_y):
        mask[ix, iy] = True

    return mask, n_rnd_x, n_rnd_y


def get_mask_ids(ids_x: np.ndarray, ids_y: np.ndarray, num_x: int, num_y: int):
    print("Input ids: ", ids_x, ids_y)
    ids = (
        set(np.argwhere(0 <= ids_x).flatten())
        & set(np.argwhere(ids_x < num_x).flatten())
        & set(np.argwhere(0 <= ids_y).flatten())
        & set(np.argwhere(ids_y < num_y).flatten())
    )
    if len(ids) == 0:
        return [], []
    ids = np.array(list(ids))
    print("Return ids:", ids_x[ids], ids_y[ids])
    return ids_x[ids], ids_y[ids]


def check_masks(m, m_padded, rnd_ids_x, rnd_ids_y):
    _ids_x, _ids_y = get_mask_ids(rnd_ids_x, rnd_ids_y, *m.shape)
    if len(_ids_x) == 0 and len(_ids_y) == 0:
        print(
            "Zero length indexes, i.e. all mask"
            " pixels found outside the mask."
        )
        return
    assert not np.all(m[_ids_x, _ids_y])
    assert np.all(m_padded[_ids_x, _ids_y])


def test_mask_padding():

    cloud_mask_padding_arr = [1, 2, 2, 2, 3, 3, 4, 5, 6]

    num_mask_px_arr = [3, 4, 4, 5, 6, 7, 20, 50, 500]
    num_x_arr = [10, 10, 15, 16, 18, 18, 50, 100, 1000]
    num_y_arr = [10, 15, 10, 16, 17, 17, 50, 100, 1000]

    for num_x, num_y, num_mask_px, cloud_mask_padding in zip(
        num_x_arr, num_y_arr, num_mask_px_arr, cloud_mask_padding_arr
    ):

        mask, n_rnd_x, n_rnd_y = setup_dummy_data(num_x, num_y, num_mask_px)

        padded_mask = pad_mask(np.copy(mask), cloud_mask_padding)

        assert mask.shape == padded_mask.shape

        print("Cloud mask pixels: ", cloud_mask_padding)

        print("Mask:")
        print(mask)

        print("Mask padded:")
        print(padded_mask)

        check_masks(mask, padded_mask, n_rnd_x + 1, n_rnd_y)
        check_masks(mask, padded_mask, n_rnd_x - 1, n_rnd_y)
        check_masks(mask, padded_mask, n_rnd_x, n_rnd_y + 1)
        check_masks(mask, padded_mask, n_rnd_x, n_rnd_y - 1)

        check_masks(mask, padded_mask, n_rnd_x + 1, n_rnd_y + 1)
        check_masks(mask, padded_mask, n_rnd_x + 1, n_rnd_y - 1)
        check_masks(mask, padded_mask, n_rnd_x - 1, n_rnd_y + 1)
        check_masks(mask, padded_mask, n_rnd_x - 1, n_rnd_y - 1)

        assert np.all(mask[get_mask_ids(n_rnd_x, n_rnd_y, *mask.shape)])
        assert np.all(padded_mask[get_mask_ids(n_rnd_x, n_rnd_y, *mask.shape)])

        assert np.all((mask | padded_mask) == padded_mask)
